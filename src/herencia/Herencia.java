/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia;

/**
 *
 * @author Pc 3
 */
public class Herencia {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Hijo hijo1 = new Hijo();
        Hijo hijo2 = new Hijo();
        Hijo hijo3 = new Hijo();
        Hijo hijo4 = hijo1;
        
        System.out.println(hijo1.toString());
        hijo2.setNombre("Franco");
        System.out.println(hijo2.toString());
        hijo3.setNombre("Tomás");
        hijo3.setApellido("AAA");
        System.out.println(hijo3.toString());
        System.out.println(hijo4.toString());
        System.out.println(hijo2.toString());
    }
    
}
